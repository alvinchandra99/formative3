import java.io.FileWriter;   // Import the FileWriter class
import java.io.IOException;  // Import the IOException class to handle errors

public class Main {

    public static void main(String[] args) {
        for(int i=1; i<=10 ; i ++){
            try {
                FileWriter myWriter = new FileWriter("form"+String.valueOf(i)+"/form"+String.valueOf(i)+".txt");
                myWriter.write("Hello Java "+String.valueOf(i));
                myWriter.close();
                System.out.println("Successfully wrote to the file.");
              } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
              }
      
        }
        
    }
    
}
